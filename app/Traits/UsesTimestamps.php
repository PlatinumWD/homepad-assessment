<?php

namespace App\Traits;

trait UsesTimestamps
{
    public function initializeUsesTimestampsTrait()
    {
        array_merge(['created_at', 'updated_at']);

        array_merge($this->hidden, ['created_at', 'updated_at']);
    }

}