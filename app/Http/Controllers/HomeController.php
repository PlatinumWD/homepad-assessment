<?php

namespace App\Http\Controllers;

use App\Http\Resources\PackageResource;
use App\Models\Package;

class HomeController extends Controller
{
    public function index()
    {
        return view('packages-list');
    }

    public function list()
    {
        return PackageResource::collection(Package::paginate(3));
    }
}
