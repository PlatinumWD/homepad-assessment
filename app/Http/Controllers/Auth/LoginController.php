<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginCustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Services\Auth\CustomerService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request as HttpRequest;

class LoginController extends Controller
{
    public CustomerService $service;

    public function __construct(CustomerService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('Auth.login');
    }

    public function login(LoginCustomerRequest $request)
    {
        $user = $this->service->login($request->validated());

        if (is_null($user) === true) {
            throw ValidationException::withMessages(['email' => [__('auth.failed')]]);   
        }

        return new CustomerResource($user);   
    }

    public function logout(HttpRequest $request)
    {
        Auth::guard('customer')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->back();
    }
}
