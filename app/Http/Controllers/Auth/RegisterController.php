<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterCustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Services\Auth\CustomerService;

class RegisterController extends Controller
{
    public CustomerService $service;

    public function __construct(CustomerService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('Auth.register');
    }

    public function create(RegisterCustomerRequest $request): CustomerResource
    {
        $user = $this->service->create($request->validated());

        return new CustomerResource($user);
    }
}
