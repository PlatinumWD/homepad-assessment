<?php

namespace App\Models;

use App\Traits\UsesTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory, UsesTimestamps;

    protected $fillable = ['name', 'limit'];

    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }
}
