<?php

namespace App\Models;

use App\Traits\UsesTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    use HasFactory, UsesTimestamps;

    protected $fillable = ['customer_id', 'package_id'];
}
