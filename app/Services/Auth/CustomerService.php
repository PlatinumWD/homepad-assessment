<?php

namespace App\Services\Auth;

use App\Models\Customer;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerService
{
    /**
     * @param array $data
     * 
     * @return Customer|string
     */
    public function create(array $data): Customer|string
    {
        try {
            DB::beginTransaction();

            $user = Customer::create(array_merge($data, [
                'password'  => bcrypt($data['password'])
            ]));

            DB::commit();

            return $user;

        } catch (Exception $e) {
            DB::rollBack();

            Log::info($e->getMessage());

            return $e->getMessage();
        }
    }

    /**
     * @param array $data
     * 
     * @return Customer|null
     */
    public function login(array $data): Customer|null
    {
        // We can retrieve the customer by the email address
        // Because the validation specifies a "unique" rule
        $user = Customer::where('email', $data['email'])->first();

        if (Auth::guard('customer')->attempt(['email' => $data['email'], 'password' => $data['password']]) === true) {
            Auth::guard('customer')->login($user);
        }

        return $user;
    }
}