<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::middleware('guest:customer')->group(function () {
    Route::get('/registration', [RegisterController::class, 'index'])->name('registration.index');
    Route::get('/login', [LoginController::class, 'index'])->name('login.index');
});

Route::middleware(['auth:sanctum,customer'])->group(function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});