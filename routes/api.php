<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// Since it's a registration route, it doesn't need any token authentication
// It could also have been put in the web.php route file
Route::middleware('guest:customer')->group(function () {
    Route::post('/create-user', [RegisterController::class, 'create'])->name('register.create');
    Route::post('/login-user', [LoginController::class, 'login'])->name('login.post');
});

Route::get('/packages', [HomeController::class, 'list'])
    ->name('packages.list')
    ->middleware('auth:sanctum,customer');