import axios from "axios";

const packagesContainer = document.getElementById('packages-container');

document.addEventListener('DOMContentLoaded', async () => {
    const response = await axios.get('/api/packages');
    console.log(response)

    if (response.status === 200) {
        packagesContainer.innerHTML = '';

        response.data.data.map(element => {
            return packagesContainer.innerHTML += `
                <div class="p-4 text-center bg-white rounded-lg flex items-center justify-between space-x-8">
                    <div class="flex-1 flex justify-between items-center">
                        <p class="h-8 w-48 bg-gray-300 rounded">${element.name}</div>
                        <div class="w-24 h-12 rounded-lg bg-purple-300">Available: ${element.isAvailable}</div>
                    </div>
                </div>
            `;
        })

        packagesContainer.innerHTML += `
            <div class="bg-white p-4 flex w-1/3">
                ${
                    Object.values(response.data.links).map((element, index) => {
                        return `
                            <div class="p-2 border-l-black"><a href="${element}">${index + 1}</a></div>
                        `;
                    })
                }
            </div>
        `;
    }
});