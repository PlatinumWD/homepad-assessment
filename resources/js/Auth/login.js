import axios from "axios";

const form              = document.getElementById('login-form');
const successContainer  = document.getElementById('success-container');
const errorsContainer   = document.getElementById('errors-container');
const data              = {};

form.addEventListener('change', (e)  => data[e.target.name] = e.target.value)

form.addEventListener('submit', async (e) => {
    e.preventDefault();
    
    try {
        await axios.get('/sanctum/csrf-cookie');
        await axios.post(form.dataset.action, data);
        successContainer.classList.remove('hidden');
        successContainer.innerHTML = `
            You logged in successfully! <br/>
            Redirecting to home page ....
        `;

        setTimeout(() => {
            window.location = '/';
        }, 3000);

    } catch (e) {
        const error = e.response.data.message;
        errorsContainer.classList.remove('hidden');
        errorsContainer.innerHTML = `${error}<br />`;

        setTimeout(() => {
            errorsContainer.classList.add('hidden');
        }, 5000);
    }
});