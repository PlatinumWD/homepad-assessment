import axios from "axios";

const form              = document.getElementById('registration-form');
const successContainer  = document.getElementById('success-container');
const errorsContainer   = document.getElementById('errors-container');
const data              = {};

form.addEventListener('change', (e)  => data[e.target.name] = e.target.value)

form.addEventListener('submit', async (e) => {
    e.preventDefault();
    
    try {
        const response = await axios.post(form.dataset.action, data)
        successContainer.classList.remove('hidden');
        successContainer.innerHTML = `
            Thank you <span class="bold">${response.data.data.name}</span> <br/>
            Your account was created successfully! <br/>
            Redirecting to login page ....
        `;

        setTimeout(() => {
            window.location = '/login';
        }, 5000);
        
    } catch (e) {
        const errors = Object.values(e.response.data.errors);
        errorsContainer.classList.remove('hidden');
        errors.map(error => errorsContainer.innerHTML = `${error[0]}<br />`);

        setTimeout(() => {
            errorsContainer.classList.add('hidden');
        }, 5000);
    }

});