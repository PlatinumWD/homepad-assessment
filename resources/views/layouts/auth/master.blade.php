<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }}</title>

        @yield('styles')
        
        @vite(['resources/sass/app.scss'])
    </head>
    <body>
        @yield('navbar')
        
        <main role="main">
            @yield('content')
        </main>

        @yield('scripts')
    </body>
</html>
