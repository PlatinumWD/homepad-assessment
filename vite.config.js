import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss', 
                'resources/js/app.js',
                'resources/js/Auth/register.js',
                'resources/js/Auth/login.js',
                'resources/js/custom/packages.js',
            ],
            refresh: true,
        }),
    ],
});
